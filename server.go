package main

import (
	"fmt"
	"go-apiboilerplate/routes"
	"go-apiboilerplate/utils"
	"os"
	"runtime"
	"strconv"

	routing "github.com/qiangxue/fasthttp-routing"
	"github.com/valyala/fasthttp"
)

var server = &fasthttp.Server{}
var router *routing.Router

func main() {
	SetGoMaxProcs()

	router = routing.New()

	InitServices()

	InitAPIs()

	listenAPI(router)
}

func SetGoMaxProcs() {
	if os.Getenv("GO_MAX_PROCS") != "" {
		maxProcs, err := strconv.Atoi(os.Getenv("GO_MAX_PROCS"))
		if err != nil {
			runtime.GOMAXPROCS(maxProcs)
		}
	}
}

func listenAPI(router *routing.Router) {
	listeningPort := os.Getenv("PORT")

	if listeningPort == "" {
		listeningPort = "8080"
	}

	fmt.Println("Listening on port " + listeningPort)
	panic(fasthttp.ListenAndServe(":"+listeningPort, CORSHandle))
}

func InitServices() {
	// Initialize services
}

func InitAPIs() {
	routes.InitAPIRoutes(router)
}

var (
	corsAllowHeaders     = "access-control-allow-origin, Content-Type, Authorization"
	corsAllowMethods     = "HEAD, GET, POST, PUT, DELETE, OPTIONS"
	corsAllowOrigin      = "*"
	corsAllowCredentials = "true"
)

func CORSHandle(ctx *fasthttp.RequestCtx) {
	ctx.Response.Header.Set("Access-Control-Allow-Headers", corsAllowHeaders)
	ctx.Response.Header.Set("Access-Control-Allow-Methods", corsAllowMethods)
	ctx.Response.Header.Set("Access-Control-Allow-Origin", corsAllowOrigin)
	ctx.Response.Header.Set("Content-Type", "application/json")

	beginTime := utils.CurrentTimeMilliseconds()
	router.HandleRequest(ctx)
	service := ctx.Response.Header.Peek("service")

	if string(ctx.Request.Header.Peek("Connection")) != "keep-alive" {
		defer ctx.Response.SetConnectionClose()
	}
	elapsedTime := utils.CurrentTimeMilliseconds() - beginTime
	fmt.Println("Elasped Time: " + strconv.FormatInt(elapsedTime, 10) + " ms ; Service = " + string(service))
}
