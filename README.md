# go-apiboilerplate

Go API boilerplate using fasthttp

## Folder Structure

* *controllers/* - where to add endpoint base handlers
* *models/* - where to add data models
* *routes/* - where to add api routes and link them to the controller
* *http/* - http related utils
* *utils/* - other utils
* server.go - start point
