package routes

import (
	"go-apiboilerplate/controllers"

	routing "github.com/qiangxue/fasthttp-routing"
)

var EXAMPLE_GROUP = "/example"

func InitExampleRoute(router *routing.Router) {
	exampleApi := router.Group(EXAMPLE_GROUP)

	exampleApi.Get("/api", controllers.GetExample)
}
