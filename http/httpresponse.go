package http

import (
	routing "github.com/qiangxue/fasthttp-routing"
)

func Response(c *routing.Context, response string, statuscode int, service string, contentType string) {
	c.Response.SetBody([]byte(response))
	c.Response.Header.SetContentType(contentType)
	c.Response.Header.Set("service", service)
	c.Response.Header.SetStatusCode(statuscode)
}
