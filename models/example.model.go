package models

type Example struct {
	Id      int
	Message string
}

func (ex *Example) FormatMessage() string {
	return `Message: ` + ex.Message
}
