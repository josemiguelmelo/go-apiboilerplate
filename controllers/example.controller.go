package controllers

import (
	"encoding/json"
	"go-apiboilerplate/http"
	"go-apiboilerplate/models"

	routing "github.com/qiangxue/fasthttp-routing"
)

var SERVICE_NAME = "ExampleService"

func GetExample(c *routing.Context) error {
	exampleModel := models.Example{
		Id:      1,
		Message: "ok",
	}
	exampleModel.Message = exampleModel.FormatMessage()
	exampleModelJSON, err := json.Marshal(exampleModel)

	if err != nil {
		http.Response(c, `{"error":true, "msg": "`+err.Error()+`"}`, 400, SERVICE_NAME, "application/json")
	}
	http.Response(c, string(exampleModelJSON), 200, SERVICE_NAME, "application/json")
	return nil
}
